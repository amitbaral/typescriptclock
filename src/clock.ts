export interface clocker {
  template: string;
}

export enum MonthList {
  January,
  February,
  March,
  April,
  May,
  June,
  July,
  August,
  September,
  October,
  November,
  December,
}

export enum dayNames {
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
}

export class Clock {
  public template: string;
  public playStatus: boolean;
  private timmer: number;

  constructor({ template }: clocker) {
    this.template = template;
    this.playStatus = false;
    this.timmer = 0;
  }

  render(): void {
    const element = document.getElementById("counter");

    let date = new Date();

    let today: string | number = date.getDay();
    console.log(today);
    let thisMonth: string | number = date.getMonth();

    const day = this.setDay(today);
    const month = this.setMonth(thisMonth);

    console.log(day, month);

    let hours: string | number = date.getHours();
    if (hours < 10) hours = `0${hours}`;

    let mins: string | number = date.getMinutes();
    if (mins < 10) mins = `0${mins}`;

    let secs: string | number = date.getSeconds();
    if (secs < 10) secs = `0${secs}`;

    let output = this.template
      .replace("h", hours.toString())
      .replace("m", mins.toString())
      .replace("s", secs.toString());

    if (element) element.innerHTML = output;
  }

  start(): void {
    this.render();
    this.timmer = setInterval(() => this.render(), 1000);
    this.playStatus = true;
  }

  stop(): void {
    clearInterval(this.timmer);
    this.playStatus = false;
  }

  setDay(day: number) {
    return dayNames[day];
  }

  setMonth(month: number) {
    return MonthList[month];
  }
}
let clock = new Clock({ template: "h:m:s" });

clock.start();

const elementButton = document.getElementById("btn");
var button = document.createElement("BUTTON");
button.innerHTML = "STOP THIS SHIT";
button.className = "awesome btn";

button.addEventListener("click", function () {
  clock.playStatus ? clock.stop() : clock.start();
});

if (elementButton) elementButton.append(button);
